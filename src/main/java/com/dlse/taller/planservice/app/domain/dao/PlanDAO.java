package com.dlse.taller.planservice.app.domain.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dlse.taller.planservice.app.domain.entity.PlanEntity;

@Repository
public interface PlanDAO extends CrudRepository<PlanEntity, Long>{
	
	public PlanEntity findByKey(String key);

}
