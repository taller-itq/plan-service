package com.dlse.taller.planservice.app.svc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dlse.taller.planservice.app.domain.entity.PlanEntity;
import com.dlse.taller.planservice.app.domain.repository.PlanRepository;
import com.dlse.taller.planservice.app.model.Plan;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlanServiceImpl implements PlanService {
	
	private final PlanRepository repository;
	
    private static final String WARNING_MSG = "Warning: {}";

	@Override
	public Plan create(Plan plan) {
		PlanEntity entity = repository.save(null);
		return null;
	}

	@Override
	public Plan findByKey(String key) {
		PlanEntity entity = repository.findByKey(null);
		return null;
	}

}
