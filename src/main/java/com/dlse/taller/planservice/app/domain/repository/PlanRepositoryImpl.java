package com.dlse.taller.planservice.app.domain.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dlse.taller.planservice.app.domain.dao.PlanDAO;
import com.dlse.taller.planservice.app.domain.entity.PlanEntity;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlanRepositoryImpl implements PlanRepository {
	
	private final PlanDAO dao;
	
    private static final String ERROR_MSG = "error: {} {} {}";
	
	@Override
	public PlanEntity save(PlanEntity plan) {
		return dao.save(plan);
	}

	@Override
	public PlanEntity findByKey(String key) {
		return dao.findByKey(key);
	}
	
	

}
