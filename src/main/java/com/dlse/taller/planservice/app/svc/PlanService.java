package com.dlse.taller.planservice.app.svc;

import com.dlse.taller.planservice.app.model.Plan;

public interface PlanService {
	
	public Plan create(Plan plan);
	
	public Plan findByKey(String key);

}
