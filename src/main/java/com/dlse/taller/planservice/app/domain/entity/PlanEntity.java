package com.dlse.taller.planservice.app.domain.entity;

import java.util.Date;

import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.UUID;

import com.dlse.taller.planservice.util.DBHelper;

import jakarta.annotation.Generated;
import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static org.hibernate.generator.EventTypeSets.UPDATE_ONLY;
import static org.hibernate.generator.EventTypeSets.INSERT_ONLY;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(
        name = "products",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "key")
        }
)
@EntityListeners(DBHelper.class)
public class PlanEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Nullable
    private Long id;

    @Nullable
    @Column(updatable = false, nullable = false, unique = true)
    @UUID
    private String key;

    @NotEmpty
    private String name;

    @NotNull
    @Range(min = 0)
    private Double price;
    
    @NotNull
    @Range(min = 0)
    private Double duration;

    @Generated("INSERT")
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @Nullable
    private Date createdAt;

    @Generated("UPDATE")
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @Nullable
    private Date updatedAt;


}
