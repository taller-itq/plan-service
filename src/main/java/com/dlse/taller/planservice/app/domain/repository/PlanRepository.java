package com.dlse.taller.planservice.app.domain.repository;

import com.dlse.taller.planservice.app.domain.entity.PlanEntity;

public interface PlanRepository {
	
	public PlanEntity save(PlanEntity plan);
	
	public PlanEntity findByKey(String key);

}
