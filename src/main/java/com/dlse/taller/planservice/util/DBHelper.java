package com.dlse.taller.planservice.util;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.dlse.taller.planservice.app.domain.entity.PlanEntity;

import jakarta.persistence.PrePersist;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DBHelper {

    /**
     * automatic property set before any database persistence
     */
    @PrePersist
    public void setKeyPrePersist(PlanEntity plan) {
        if (StringUtils.isBlank(plan.getKey())) {
            plan.setKey(null);
        }
    }

}

